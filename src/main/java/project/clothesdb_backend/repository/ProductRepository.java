package project.clothesdb_backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import project.clothesdb_backend.model.Product;

public interface ProductRepository extends MongoRepository<Product, String> {

}
