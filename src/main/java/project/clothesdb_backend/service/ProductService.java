package project.clothesdb_backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.clothesdb_backend.model.Product;
import project.clothesdb_backend.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public Product save(Product product) {
        return productRepository.save(product);
    }

    public Product findById(String id) {
        Optional<Product> product = productRepository.findById(id);
        return product.orElse(null);
    }

    public Product update(String id, Product productDetails) {
        Product product = findById(id);
        if (product != null) {
            product.setName(productDetails.getName());
            product.setPrice(productDetails.getPrice());
            product.setDescription(productDetails.getDescription());
            product.setBrand(productDetails.getBrand());
            product.setSize(productDetails.getSize());
            product.setCategory(productDetails.getCategory());
            product.setColor(productDetails.getColor());
            return productRepository.save(product);
        } else {
            // Handle the case when the product with the given id doesn't exist
            return null;
        }
    }

    public void delete(String id) {
        productRepository.deleteById(id);
    }
}
