package project.clothesdb_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClothesDbBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClothesDbBackendApplication.class, args);
    }

}
